#!/bin/bash
phrases=phrases.tsv
proverbs=proverbs.tsv
money=Money.csv
animals=animal.dyu.tsv
dest=./
quiz=index.adoc
tsv="quiz.tsv"
theme=julakan
export days
export months
export dows
source ./params.sh

for x in shuf; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done

#[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
#cp  "./theme/$theme.css" node_modules/reveal.js/css/theme/
#cp  ./theme/*.css node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

# Trouvez la traduction des phrases jula suivantes vers le français. +
# La flêche droite affiche d'abord un indice, et ensuite la traduction en français.
# Il y a du nouveau chaque jour, alors revenez demain! +

cat <<EOF >$quiz
= Julakan karan lon o lon !
:revealjs_theme: $theme
:title-slide-transition: zoom
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
//:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: ./custom.css
:imagesdir: ./images
:docinfo: shared
:icons: font

La fleche droite change la catégorie de questions.
La fleche en bas démarre les questions.

====
**I ni baara !**
====

EOF

printf "== Calendrier\n\n" >>$quiz

printf "=== Bi ye lon juman ye ?\n\n" >>$quiz
printf "[step=1]\n" >>$quiz
printf "bi ye [.step]#%s# ye,\n\n" "$dow" >>$quiz
echo -e '[%step]\n' >>$quiz
echo -e "[%step]#$month#, tere [.step]#$day#,\n\n" >>$quiz
echo -e "[%step]\n" >>$quiz
printf "saan [.step]#waga fila ani mugan ni naani kɔnɔ#\n\n" >>$quiz

printf "== 5 Phrases\n\n" >>$quiz
shuf -n 5 $phrases | awk -v quiz=$quiz -F"\t" '{ hint=$2 }{ print "=== ", $3, "\n" >> quiz}
{ print "[TIP,step=1]" >> quiz }
{ print hint, "\n" >> quiz}
{ print "[step=2]" >> quiz }
{ print "**"$4"**", "\n\n" >> quiz}'
printf "=== Félicitations ! !\n\n" >>$quiz
printf "Mais n'oubliez pas de revenir demain pour un nouveau jeu de phrases.\n\n" >>$quiz

#npx asciidoctor-revealjs -v -D $dest  *.adoc
printf "== 1 Proverbe\n\n" >>$quiz
shuf -n 1 $proverbs | awk -v quiz=$quiz -F"\t" '
{ print "[step=1]" >> quiz }
{ print "=== ", $1, "\n" >> quiz}
{ print "[step=2]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}'
printf "=== Mais n'oubliez pas de revenir demain pour un nouveau proverbe.\n" >>$quiz

printf "include::color.adoc[]\n\n" >>$quiz

cat <<EOF >>$quiz
== Wariko karan lon o lon !

[.r-fit-text]
Pour un étranger, l'argent en Jula n'est pas facile !
Venez chaque jour pour devenir habile au marché avec les 'darasi!'
La flêche en bas affiche le montant CFA et ensuite en Jula.

====
**I ni lɔgɔ !**
====

image::cfa.orig.jpg[background, size=cover]

EOF

csvtool -t COMMA -u TAB cat $money | grep argent |
  shuf -n 5 | awk -v quiz=$quiz -F "\t" '
{ alt=srand() }
{ if (!($6 == ""))
  {
  { print "[%notitle, background-color='#333333']" >> quiz };
  { print "=== ", $2, "\n\n" >> quiz};
  { image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6);
    print "image::"image"[background, size=cover]\n\n">> quiz
  };
  { print "[.r-fit-text,step=1]" >> quiz };
  { print "**"$5"**", "\n\n" >> quiz};
}
else {
  { print "[background-color='#28cc2d']" >> quiz };
  { print "=== ", $2, "\n\n" >> quiz};
  { print "[.r-fit-text,step=1]" >> quiz };
  { print "**"$5"**", "\n\n" >> quiz};
    }
  }'

csvtool -t COMMA -u TAB cat $money | grep argent |
  shuf -n 5 | awk -v quiz=$quiz -F "\t" '
  { alt=srand() }
  { print "[background-color='#3581D8']" >> quiz }
  { print "=== ", $5, "\n" >> quiz}
  { print "[.r-fit-text,step=1]" >> quiz }
  { print "**"$2"**", "\n\n" >> quiz}
  { if (!($6 == ""))
    { image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6);
      print "image::"image"[step=1]\n\n">> quiz }
    }'

printf "[background-color="#333333"]\n" >>$quiz
printf "=== Félicitations ! !\n\n" >>$quiz
printf "Mais n'oubliez pas de revenir demain pour un nouveau séance de 'Wari'.\n\n" >>$quiz
printf "image::cfa.orig.jpg[background, size=cover, opacity=.5]\n\n" >>$quiz

cat <<EOF >>$quiz
== Animaux
Apprenez les noms des animaux en jula de Côte d'Ivoire. +
On vous propose 6 noms d'animaux soit en français, soit en jula. +
La flêche droite affiche la traduction.
Ça change chaque jour, alors revenez demain! +

====
**I ni baara !**
====

EOF

shuf -n 3 $animals | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#28CC2D']" >> quiz }
{ print "=== ", $1, "\n" >> quiz}
{ print "[.r-fit-text, step=1]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}'

shuf -n 3 $animals | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#3581D8']" >> quiz }
{ print "=== ", $2, "\n" >> quiz}
{ print "[.r-fit-text,step=1]" >> quiz }
{ print "**"$1"**", "\n\n" >> quiz}'

printf "[transition='zoom']\n" >>$quiz
printf "=== Félicitations ! !\n\n" >>$quiz
printf "N'oubliez pas de revenir demain pour 6 autres animaux.\n" >>$quiz
printf "Mais si vous êtes impatients, la liste complète se trouve https://coastsystems.net/gxx/page/animals/[ici].\n" >>$quiz
