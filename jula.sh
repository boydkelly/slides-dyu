#!/bin/bash
in=animal.dyu.tsv
dest=./
quiz=animal.dyu.adoc
theme=julakan
export days
export months
export dows
source ./params.sh

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

[[ -d /home/bkelly/dev/web-docs/content/dyu/modules/blog/partials/ ]] && cp -v /home/bkelly/dev/web-docs/content/dyu/modules/blog/partials/animal.dyu.csv .

csvtool -u TAB -t COMMA cat animal.dyu.csv | csvtool drop 1 - > animal.dyu.tsv

[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp "./theme/$theme.css" node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz
= Animaux en Jula de Côte d'Ivoire
:revealjs_theme: $theme
:revealjs_transition: convex
:title-slide-transition: zoom
:revealjs_slidenumber: false
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:docinfo: shared
:icons: font

Apprenez les noms des animaux en Jula de Côte d'Ivoire. +
On vous propose 6 noms d'animaux soit en français, soit en Jula. +
La flêche droite affiche la traduction.
Ça change chaque jour, alors revenez demain! +

====
**an be sini !!**
====

EOF

shuf -n 3 $in | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#28CC2D']" >> quiz }
{ print "== ", $1, "\n" >> quiz}
{ print "[.r-fit-text, step=1]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}'

shuf -n 3 $in | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#3581D8']" >> quiz }
{ print "== ", $2, "\n" >> quiz}
{ print "[.r-fit-text,step=1]" >> quiz }
{ print "**"$1"**", "\n\n" >> quiz}'

printf "[transition='zoom']\n" >> $quiz
printf "== Félicitations ! !\n\n" >> $quiz
printf "N'oubliez pas de revenir demain pour 6 autres animaux.\n" >> $quiz
#printf "Mais si vous êtes impatients, la liste complète se trouve https://coastsystems.net/fr/2021/04/les-animaux-en-jula-de-c%C3%B4te-divoire/[ici].\n" >> $quiz

#npx asciidoctor-revealjs -D $dest  $quiz
