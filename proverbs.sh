#!/bin/bash
in=proverbs.tsv
dest=./
quiz=proverbs.adoc
tsv="proverbs.tsv"
theme=julakan

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp  "./theme/$theme.css" node_modules/reveal.js/css/theme/
cp  ./theme/*.css node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz 
= ntàlen kura be yan lon o lon !
:revealjs_theme: $theme
:source-highlighter: highlight.js
:title-slide-transition: zoom
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: ./custom.css
:imagesdir: ./images
:docinfo: shared
:icons: font

EOF

shuf -n 1 $in | awk -v quiz=$quiz -F"\t" '
{ print "[.r-fit-text,step=1]" >> quiz }
{ print "== ", $1, "\n" >> quiz}
{ print "[.r-fit-text,step=2]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}'

printf "== Mais n'oubliez pas de revenir demain pour un nouveau proverbe.\n" >> $quiz

#npx asciidoctor-revealjs -v -D $dest  *.adoc 

