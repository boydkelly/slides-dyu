#!/bin/bash
in=animal.gxx.tsv
dest=./
quiz=animal.gxx.adoc
theme=julakan
export days
export months
export dows
source ./params.sh

for x in shuf csvtool; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

csvtool -u TAB -t COMMA cat animal.gxx.csv | csvtool drop 1 - > animal.gxx.tsv
[[ -d /var/home/bkelly/dev/web-docs/content/gxx/modules/blog/partials ]] && cp -v animal.gxx.?sv /var/home/bkelly/dev/web-docs/content/gxx/modules/blog/partials

[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp "./theme/$theme.css" node_modules/reveal.js/css/theme/
#cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/
#cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz
= nmian an' 'ɲnɩn 'dhe ghwɛɛ'ghwlu 'dhi 
:revealjs_theme: $theme
:revealjs_transition: convex
:title-slide-transition: zoom
:revealjs_slidenumber: false
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:docinfo: shared
:icons: font

Apprenez les noms des animaux en Guéré de Côte d'Ivoire. +
On vous propose 6 noms d'animaux soit en français, soit en guéré. +
La flêche droite affiche la traduction.
Ça change chaque jour, alors revenez demain! +

====
**a 'sɛ dhbho ''bun!!**
====

EOF

shuf -n 3 $in | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#28CC2D']" >> quiz }
{ print "== ", $1, "\n" >> quiz}
{ print "[.r-fit-text, step=1]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}'

shuf -n 3 $in | awk -v quiz=$quiz -F"\t" '{ print "[background-color='#3581D8']" >> quiz }
{ print "== ", $2, "\n" >> quiz}
{ print "[.r-fit-text,step=1]" >> quiz }
{ print "**"$1"**", "\n\n" >> quiz}'

printf "[transition='zoom']\n" >> $quiz
printf "== Félicitations ! !\n\n" >> $quiz
printf "N'oubliez pas de revenir demain pour 6 autres animaux.\n" >> $quiz
printf "Mais si vous êtes impatients, la liste complète se trouve https://coastsystems.net/gxx/page/animals/[ici].\n" >> $quiz

#npx asciidoctor-revealjs -D $dest  $quiz 
